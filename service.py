from flask import Flask
from flask import request
app = Flask(__name__)

try:
    import processing
except:
    import processing_dummy as processing

@app.route("/", methods=['GET'])
def process():
    user_input = request.args.get('msg')
    
    if user_input is not None:
        return processing.process(user_input)
    return 'default message'

    
if __name__== "__main__":
    app.run()
