# This file is a template, and might need editing before it works on your project.
FROM python:3.6


WORKDIR /app

COPY requirements.txt /app/
RUN pip install --no-cache-dir -r requirements.txt

COPY . /app

